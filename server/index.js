
var winston = require('./winston.js');
const Joi = require('joi'); 
const express = require('express')
const app = express();
const morgan = require('morgan')
var bodyParser = require('body-parser');
app.use(morgan('combined', { stream: winston.stream }));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

const Sequelize = require("sequelize");
// const fetch = require("node-fetch");
const sequelize = new Sequelize('mytestdb', 'ravindra', '05101997', {
    host: 'localhost',
    dialect: "mysql" // or 'sqlite', 'postgres', 'mariadb'

  

});
sequelize
    .authenticate()
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch(err => {
        console.error("Unable to connect to the database:", err);
    });
const User = sequelize.define(
    "director1", {
        director_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        id: false
    }
);
const movie = sequelize.define(
    "movie1", {
        rank: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        runtime: {
            type: Sequelize.INTEGER
        },
        genre: {
            type: Sequelize.STRING
        },
        rating: {
            type: Sequelize.STRING
        },
        metascore: {
            type: Sequelize.STRING
        },
        votes: {
            type: Sequelize.INTEGER
        },
        earnings: {
            type: Sequelize.STRING
        },
        director_id: {
            type: Sequelize.INTEGER
        },
        actor: {
            type: Sequelize.STRING
        },
        year: {
            type: Sequelize.INTEGER
        }
    }, {
        timestamps: false
    }
);

app.get('/', function (req, res,next) {
    throw new Error("Something went wrong");
    res.send('hello world')
    // next("sedjnj")
    

})
app.get('/api/movies', (req, res,next) => {
    movie.findAll().then(user => {
        res.send(user)
    }).catch(function (err) {
        // next();
        console.log("findById failed with error: " + err);
        return null;

    })


})
app.get('/api/movies/:movieId', (req, res) => {

    movie.findAll({
        where: {
            rank: req.body.movieId
        }
    }).then(user => {
        res.send(user)
    }).catch(function (err) {
        next(err)
        console.log("findById failed with error: " + err);
        return null;

    })

})
app.post('/api/movies', (req, res,next) => {


    let data = req.body
    movie.create({
        "id": data["id"],
        "rank": data["rank"],
        "title": data["title"],
        "description": data['description'],
        "runtime": data['runtime'],
        "genre": data['genre'],
        "rating": data['rating'],
        "metascore": data['metascore'],
        "votes": data['votes'],
        "earnings": data['earnings'],
        "director_id": data['director_id'],
        "actor": data['actor'],
        "year": data['year']
    }).then(user => {
        res.send('sucsess')
    }).catch(function (err) {
       
        console.log("findById failed with error: " + err);
        return null;

    })

})
app.put('/api/movies/:movieId', (req, res) => {


    let data = req.body
    movie.update({
        title: data["title"]
    }, {
        where: {
            id: req.body.id
        }
    }).then(user => {
        res.send('sucsess')
    }).catch(function (err) {
        console.log("findById failed with error: " + err);
        return null;

    })

})
app.delete('/api/movies/:movieId', (req, res) => {
    const id = req.params.movieId;
    movie.destroy({
            where: {
                rating: id
            }
        })
        .then(deletedOwner => {
            res.send("deleted");
        });
})
app.get('/api/directors', (req, res) => {
    User.findAll()
        // .then((movie1s) => {
        // res.json(movie1s)
        // })
        .then(user => {
            res.send(user)
        }).catch(function (err) {
            console.log("findById failed with error: " + err);
            return null;

        })

})
app.post('/api/directors', (req, res) => {


    let data = req.body
    User.create({
        director_id: data["id"],
        name: data["name"]

    }).then(user => {
        res.send('sucsess')
    }).catch(function (err) {
        console.log("findById failed with error: " + err);
        return null;

    })

})
app.put('/api/directors/:directorId', (req, res) => {


    let data = req.body
    User.update({
        name: data["name"]
    }, {
        where: {
            director_id: data['id']
        }
    }).then(user => {
        res.send('sucsess')
    }).catch(function (err) {
        console.log("findById failed with error: " + err);
        return null;

    })

})
app.delete('/api/directors/:directorId', (req, res) => {
    User.destroy({
            where: {
                director_id: req.body.id
            }
        })
        .then(deletedOwner => {
            res.send("deleted");
        });
})

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
  });

app.use((error, req, res, next) => {
    res.status(error.status || 500).send({
      error: {
        status: error.status || 500,
        message: error.message || 'Internal Server Error',
      },
    });
  });
  







app.listen(8000, () => {
    console.log('server started')
})