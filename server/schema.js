const schema = Joi.object().keys({
    name: Joi.string().alphanum().min(3).max(30).required(),
    birthyear: Joi.number().integer().min(1970).max(2013),
    Rank: Joi.number().integer(),
    Title: Joi.alphanum().min(3).max(30).required(),
    Description: Joi.alphanum().min(10).max(),
    Runtime: Joi.number(),
    Genre: Joi.string(),
    Rating: Joi.number(),
    Metascore: Joi.number(),
    Votes: Joi.number(),
    Gross_Earning_in_Mil: Joi.number(),
    Director: Joi.string(),
    Actor: Joi.string(),
    Year: Joi.number()
});