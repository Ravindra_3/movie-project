const Sequelize = require("sequelize");
const fetch = require("node-fetch");
const sequelize = new Sequelize('mytestdb', 'ravindra', '05101997', {
        host: 'localhost',
        dialect: "mysql" // or 'sqlite', 'postgres', 'mariadb'
//         //
});
sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });
const User = sequelize.define(
  "director1",
  {
    director_id: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    name: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: false,
    id: false
  }
);
const movie = sequelize.define(
  "movie1",
  {
    rank: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    title: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    runtime: {
      type: Sequelize.INTEGER
    },
    genre: {
      type: Sequelize.STRING
    },
    rating: {
      type: Sequelize.STRING
    },
    metascore: {
      type: Sequelize.STRING
    },
    votes: {
      type: Sequelize.INTEGER
    },
    earnings: {
      type: Sequelize.STRING
    },
    director_id: {
      type: Sequelize.INTEGER
    },
    actor: {
      type: Sequelize.STRING
    },
    year: { type: Sequelize.INTEGER }
  },
  {
    timestamps: false
  }
);
async function makeTables() {
  
    await User.sync({ force: true })
   let result =   await fetch(
        "https://trello-attachments.s3.amazonaws.com/5e50ee8e7080a17e9756d05f/5e6727c95cc47409c0f7f995/x/30cb8168802601d7e92ea2ecd1463c9a/movies.json"
      )
      let obj =  await result.json()
        
          obj.forEach(object => {
            if (object.rating == "NA") {
              object.rating = 0;
            }
          })
          var i = 1;
          var values1 = obj.reduce((acc, current) => {
            if (acc[current["Director"]] == undefined) {
              acc[current["Director"]] = i;
              i++;
            }
            return acc;
          }, {});
     await  new Promise((resolve) =>{
          for (i in values1) {
              User.create({
              director_id: `${values1[i]}`,
              name: i
            });
          }
          resolve(User)
        })
        
    
  
  
   await movie.sync({ force: true })
 let result1 = await  fetch(
        "https://trello-attachments.s3.amazonaws.com/5e50ee8e7080a17e9756d05f/5e6727c95cc47409c0f7f995/x/30cb8168802601d7e92ea2ecd1463c9a/movies.json"
      )
     let obj1 =    await result1.json()
     let found;
    
          obj1.map(async key => {
         found =    await User.findAll({
              attributes: ["director_id"],
              where: {
                name: key["Director"]
              }
            })
              key['Director'] = found[0]['dataValues']['director_id']
                 movie.create({
                       rank: key['Rank'],
                       title: key['Title'],
                       description: key['Description'],
                       runtime: key['Runtime'],
                       genre : key['Genre'],
                       rating : key['Rating'],
                       metascore : key['Metascore'],
                       votes : key['Votes'],
                       earnings: key['Gross_Earning_in_Mil'],
                       director_id : key['Director'],
                       actor : key['Actor'],
                       years : key["Year"]
                 })
             }) 
             }
makeTables();
module.exports = { User,movie};